import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTextFieldOutline extends StatefulWidget {

  final String label;
  final void Function(String value) onSaved;
  final Icon icon;
  final TextInputType keyboardType ;
  final bool password ;

  const CustomTextFieldOutline({Key key, this.label, this.onSaved, this.icon , this.keyboardType ,this.password})
      : super(key: key);

  @override
  _CustomTextFieldOutlineState createState() => _CustomTextFieldOutlineState();
}

class _CustomTextFieldOutlineState extends State<CustomTextFieldOutline> {



  TextEditingController controller = TextEditingController();



  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: widget.keyboardType,
      cursorColor: Colors.red,
      cursorWidth: 2.0,
      maxLines: 1,
      obscureText: widget.password,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'لطفا فیلد را پر کنید.';
        }
        return null;
      },

      style: TextStyle(
        fontSize: 14.0,
        color: Colors.red,

      ),
      onSaved: widget.onSaved,
      //textDirection: TextDirection.rtl,
      textAlign: TextAlign.right,
      decoration: InputDecoration(
        border:  OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red, width: 2.0),
          borderRadius:BorderRadius.circular(25.0),
        ),

        prefixIcon: Icon(widget.icon.icon, color: Colors.orange[800],),
        labelText: widget.label,
        labelStyle: TextStyle(color: Colors.red , fontSize: 14, fontWeight: FontWeight.w600),
        focusColor: Colors.red,
        fillColor: Colors.white.withOpacity(0.8),
        filled: true,

      ),
    );
  }
}