import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form2/Models/post.dart';
import 'package:form2/service/post_service.dart';
import 'package:form2/widgets/textArea.dart';
import 'package:http/http.dart';
import '../main.dart';
import '../post_details.dart';
import 'custom_button_filled_red.dart';
import 'custom_button_white_filled.dart';
import 'custom_text_field_outline.dart';

class EditPostForm extends StatefulWidget {
  const EditPostForm({Key key, this.id, this.title,this.body, this.userId}) : super(key: key);
  final String title;
  final String body;
  final int id;
  final int userId;

  @override
  _EditPostFormState createState() => _EditPostFormState();
}

class _EditPostFormState extends State<EditPostForm> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  var title, body;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.jpg"),
              fit: BoxFit.cover,
              //colorFilter: ColorFilter.mode(Colors.white54, BlendMode.hue),
            ),
          ),
          child: Padding(
            key: _scaffoldKey,
            padding: const EdgeInsets.all(30.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 10),
                    child: CustomTextFieldOutline(
                      label: "عنوان پست",
                      icon: Icon(Icons.title),
                      keyboardType: TextInputType.text,
                      password: false,
                      onSaved: (String str) {
                        title = str;
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 5, 20, 20),
                    child: CustomTextArea(
                      label: 'متن ...',
                      onSaved: (text) {
                        body = text;
                      },
                      icon: Icon(Icons.text_fields),
                    ),
                  ),
                  CustomButtonRedFilled(
                    label: "ویرایش",
                    onPressedDo: () async {
                      _formKey.currentState.save();
                      var response = await PostService.updatePost(new Post(
                          widget.userId,
                          widget.id,
                          title,
                          body));
                      Navigator.push(context, MaterialPageRoute(builder: (context) {
                        return PostDetail(id: widget.id ,title: title, body: body ,  userId: widget.userId,);
                      }));
                    },

                  ),
                  CustomButtonWhiteFilled(
                    label: "بازگشت",
                    onPressedDo: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MyHomePage()),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
