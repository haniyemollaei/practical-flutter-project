import 'package:flutter/material.dart';

class CustomTextArea extends StatefulWidget {
  final String label;
  final void Function(String value) onSaved;
  final Icon icon;

  const CustomTextArea({Key key, this.label, this.onSaved, this.icon ,})
      : super(key: key);

  @override
  _CustomTextAreaState createState() => _CustomTextAreaState();
}

class _CustomTextAreaState extends State<CustomTextArea> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.text,
      cursorColor: Colors.red,
      cursorWidth: 2.0,
      maxLines: 8,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'لطفا فیلد را پر کنید.';
        }
        return null;
      },

      style: TextStyle(
        fontSize: 14.0,
        color: Colors.indigo,
      ),
      onSaved: widget.onSaved,
      textDirection: TextDirection.rtl,
      textAlign: TextAlign.right,
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.auto,
        hintTextDirection: TextDirection.rtl,
        border:  OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red, width: 2.0),
          borderRadius:BorderRadius.circular(10.0),
        ),

        prefixIcon: Icon(widget.icon.icon, color: Colors.orange[800],),
        labelText: widget.label,
        labelStyle: TextStyle(color: Colors.red , fontSize: 14, fontWeight: FontWeight.w600),
        focusColor: Colors.red,
        fillColor: Colors.white.withOpacity(0.9),
        filled: true,

      ),
    );
  }
}
