import 'package:flutter/material.dart';

import '../send_posts.dart';

class CustomButtonRedFilled extends StatefulWidget {

  final String label;
  final void Function() onPressedDo;
  const CustomButtonRedFilled({Key key, this.label, this.onPressedDo}) : super(key: key);

  @override
  _CustomButtonRedFilledState createState() => _CustomButtonRedFilledState();
}

class _CustomButtonRedFilledState extends State<CustomButtonRedFilled> {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(30.0),
      ),
      height: 40.0,
      minWidth: 150.0,
      color: Colors.red,
      textColor: Colors.white,
      child: new Text(widget.label,
          style: TextStyle(fontFamily: 'IranSans')),
      splashColor: Colors.redAccent,
      onPressed: widget.onPressedDo,
    );
  }
}
