import 'package:flutter/material.dart';

class CustomButtonWhiteFilled extends StatefulWidget {
  final String label;
  final void Function() onPressedDo;
  const CustomButtonWhiteFilled({Key key, this.label, this.onPressedDo}) : super(key: key);

  @override
  _CustomButtonWhiteFilledState createState() => _CustomButtonWhiteFilledState();
}

class _CustomButtonWhiteFilledState extends State<CustomButtonWhiteFilled> {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(30.0),
      ),
      height: 40.0,
      minWidth: 150.0,
      color: Colors.white,
      textColor: Colors.red,
      child: new Text(widget.label,
          style: TextStyle(fontFamily: 'IranSans')),
      splashColor: Colors.redAccent,
      onPressed: widget.onPressedDo,
    );
  }
}
