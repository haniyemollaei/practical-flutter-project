import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form2/Models/image.dart';
import 'package:form2/service/image_file_service.dart';

class Gallery extends StatefulWidget {
  const Gallery({Key key}) : super(key: key);

  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.red,
      ),
      child: FutureBuilder<List<CustomImage>>(
        future: ImageFileService.getAllImages(),
        initialData: [],
        builder: (context, AsyncSnapshot<List<CustomImage>> snapshot) {
          return snapshot.hasData
            ? GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
              ) ,
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  color: Colors.white ,
                  child: Center(child: Image.file(File(snapshot.data[index].path))),
                );
              }
          ) : Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
