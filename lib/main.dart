import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:form2/contact_list.dart';
import 'package:form2/send_posts.dart';
import 'package:form2/widgets/custom_button_filled_red.dart';
import 'package:form2/widgets/custom_button_white_filled.dart';
import 'camera.dart';
import 'gallery.dart';
import 'gallery2.dart';
import 'log_in.dart';
import 'register.dart';

Future<void> main() async {


  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Form Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
        fontFamily: 'Koodak',
      ),
      home: MyHomePage(title: 'Form Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.jpg"),
            fit: BoxFit.cover,
            //colorFilter: ColorFilter.mode(Colors.white54, BlendMode.hue),
          ),
        ),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 150, 30, 0),
            child: Column(children: <Widget>[

              CustomButtonWhiteFilled(
                label: "ثبت نام",
                onPressedDo: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Register()),
                  );
                },
              ),

              CustomButtonWhiteFilled(
                label: "ورود",
                onPressedDo: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LogIn()),
                  );
                },
              ),

              CustomButtonRedFilled(
                label: "اضافه کردن پست",
                onPressedDo: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SendPost()),
                  );
                },
              ),
              CustomButtonRedFilled(
                label: "دوربین",
                onPressedDo: () async {

                  WidgetsFlutterBinding.ensureInitialized();

// Obtain a list of the available cameras on the device.
                  final cameras = await availableCameras();

// Get a specific camera from the list of available cameras.
                  final firstCamera = cameras.first;

                  await Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CameraPage(firstCamera,)),
                  );
                },
              ),
              CustomButtonWhiteFilled(
                label: "گالری",
                onPressedDo: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Gallery()),
                  );
                },
              ),
              CustomButtonWhiteFilled(
                label: "گالری دوم",
                onPressedDo: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Gallery2()),
                  );
                },
              ),
            ]),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ContactList()),
          );
        },
        child: const Icon(Icons.contact_mail),
        backgroundColor: Colors.amber,
      ),
    );
  }
}
