import 'package:flutter/material.dart';
import 'package:form2/api_users.dart';
import 'package:form2/main.dart';
import 'package:form2/service/toast.dart';
import 'package:form2/service/user_service.dart';
import 'package:form2/widgets/custom_button_filled_red.dart';
import 'package:form2/widgets/custom_button_white_filled.dart';
import 'package:form2/widgets/custom_text_field_outline.dart';


class LogIn extends StatefulWidget {
  @override
  LogInState createState() {
    return LogInState();
  }
}

class LogInState extends State<LogIn> {

  final _formKey = GlobalKey<FormState>();
  String username,  password;


  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.jpg"),
              fit: BoxFit.cover,
              //colorFilter: ColorFilter.mode(Colors.white54, BlendMode.hue),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(30.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[


                  ///////////////////////////////////////  username  //////////////////////////////////////
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    child: CustomTextFieldOutline(
                      label: "نام کاربری",
                      icon: Icon(Icons.person , color: Colors.orange,),
                      keyboardType: TextInputType.text,
                      password: false,
                      onSaved: (String str){
                        username = str;
                      },
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    child: CustomTextFieldOutline(
                      label: "رمز عبور",
                      icon: Icon(Icons.workspaces_outline , color: Colors.orange,),
                      keyboardType: TextInputType.text,
                      password: true,
                      onSaved: (String str){
                        password = str;
                      },
                    ),
                  ),


                 CustomButtonRedFilled(
                    label: "ورود",
                    onPressedDo: () async {
                      if (_formKey.currentState.validate()) {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(SnackBar(content: Text('اطلاعات در حال پردازش است.')));
                        print("logged in");
                        //await getData();
                        var users2 = await UserService.fetchUsers2();
                        users2.forEach((element) {
                          print( "${element.username}     ");
                        });
                        print("logged in2");
                        ShowToastComponent.showDialog("خوش آمدید",  context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => APIUsers()),
                        );
                      }
                    },
                  ),

                  CustomButtonWhiteFilled(
                    label: "بازگشت",
                    onPressedDo: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MyHomePage()),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}