import 'dart:typed_data';

class ImageByte {
  int id;
  Uint8List imageBytes;

  ImageByte({this.id, this.imageBytes});

  ImageByte.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageBytes = json['image_bytes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image_bytes'] = this.imageBytes;
    return data;
  }
}