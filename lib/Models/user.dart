class User {
  int id;
  String email;
  String username;
  String password;

  User(this.id, this.email, this.username, this.password);

  User.fromJson(dynamic obj) { //from json
    this.id = obj['id'];
    this.email = obj['email'];
    this.username = obj['username'];
    this.password = obj['password'];
  }


  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["username"] = username;
    map["password"] = password;
    map["email"] = email;
    return map;
  }
}