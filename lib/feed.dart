import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form2/Models/post.dart';
import 'package:form2/Models/user.dart';
import 'package:form2/post_details.dart';
import 'package:form2/send_posts.dart';
import 'package:form2/service/post_service.dart';
import 'package:form2/service/user_service.dart';

class Feed extends StatefulWidget {
  const Feed({Key key}) : super(key: key);

  @override
  _FeedState createState() => _FeedState();
}

class _FeedState extends State<Feed> {
  @override
  build(BuildContext context) {

    // Build a Form widget using the _formKey created above.
    return MaterialApp(
      home: Container(
        child: Scaffold(
          appBar: AppBar(
            title: Row(
              children: [

                IconButton(
                  icon: Icon(Icons.arrow_back_outlined),
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SendPost()),
                    );
                  },
                ),

                Text('پست ها' , textAlign: TextAlign.center,
                  style: TextStyle(fontFamily: 'Lalezar'),textDirection: TextDirection.rtl ,),
              ],
            ),
            backgroundColor: Colors.red,
            centerTitle:true ,
            //leading: Icon(Icons.arrow_back_outlined),
          ),
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/background.jpg"),
                fit: BoxFit.cover,
                //colorFilter: ColorFilter.mode(Colors.white54, BlendMode.hue),
              ),
            ),
            child: FutureBuilder<List<Post>>(
                future: PostService.fetchPosts(),
                initialData: [],
                // ignore: missing_return
                builder: (context, AsyncSnapshot<List<Post>> snapshot) {
                  if(snapshot.connectionState == ConnectionState.done){
                    return ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (_, int position) {
                        final item = snapshot.data[position];
                        return Card(
                          child: ListTile(
                            title: Text(item.title , style: TextStyle(color: Colors.red),),
                            subtitle: Text(item.body) ,
                            trailing:Icon(Icons.people_outline_sharp , color: Colors.orange,),
                            onTap:(){ goToDetail(context, item.id , item.title , item.body , item.userId );},
                            tileColor: Colors.white.withOpacity(0.9),
                            contentPadding: EdgeInsets.fromLTRB(20, 8, 10, 10),
                          ),

                        );
                      },
                    );
                  }else if(snapshot.connectionState == ConnectionState.waiting){
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if(snapshot.hasError){
                    final snackBar = SnackBar(content: Text('Error'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }else{
                    return Text("nothing");
                  }
                }
            ),
          ),
        ),
      ),
    );
  }

  void goToDetail(BuildContext context,int id , String title , String body, int userId) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return PostDetail(id: id ,title: title , body: body ,  userId: userId,);
    }));
  }
}

