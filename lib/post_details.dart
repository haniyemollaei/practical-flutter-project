import 'package:flutter/material.dart';
import 'package:form2/feed.dart';
import 'package:form2/widgets/custom_button_filled_red.dart';
import 'package:form2/widgets/custom_button_white_filled.dart';
import 'package:form2/widgets/edit_post.dart';

class PostDetail extends StatefulWidget {
  const PostDetail({Key key, this.id, this.title,this.body, this.userId}) : super(key: key);
  final String title;
  final String body;
  final int id;
  final int userId;
  @override
  _PostDetailState createState() => _PostDetailState();
}

class _PostDetailState extends State<PostDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.jpg"),
            fit: BoxFit.cover,
            //colorFilter: ColorFilter.mode(Colors.white54, BlendMode.hue),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
          child: Column(
            children: [
              Card(
                child: ListTile(
                  title: Text(widget.title , style: TextStyle(color: Colors.red),),
                  subtitle: Text(widget.body) ,
                  trailing:Icon(Icons.people_outline_sharp , color: Colors.orange,),
                  tileColor: Colors.white.withOpacity(0.9),
                  contentPadding: EdgeInsets.fromLTRB(20, 8, 10, 10),
                ),
              ),
              CustomButtonRedFilled(
                label: "ویرایش",
                onPressedDo: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return EditPostForm(id: widget.id ,title: widget.title , body: widget.body ,  userId: widget.userId,);
                  }));
                },
              ),
              CustomButtonWhiteFilled(
                label: "بازگشت",
                onPressedDo: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Feed()),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
