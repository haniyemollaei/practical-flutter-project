import 'package:flutter/material.dart';
import 'package:form2/Models/user.dart';
import 'package:form2/service/user_service.dart';

class APIUsers extends StatefulWidget {
  const APIUsers({Key key}) : super(key: key);

  @override
  _APIUsersState createState() => _APIUsersState();
}

class _APIUsersState extends State<APIUsers> {
  @override
  build(BuildContext context) {

    // Build a Form widget using the _formKey created above.
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('API Users'),
        ),
        body: FutureBuilder<List<User>>(
            future: UserService.fetchUsers2(),
            initialData: [],
            // ignore: missing_return
            builder: (context, AsyncSnapshot<List<User>> snapshot) {
              if(snapshot.connectionState == ConnectionState.done){
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (_, int position) {
                    final item = snapshot.data[position];
                    return Card(
                      child: ListTile(
                        title:
                        Text("user: " + item.email),
                      ),
                    );
                  },
                );
              }else if(snapshot.connectionState == ConnectionState.waiting){
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else if(snapshot.hasError){
                final snackBar = SnackBar(content: Text('Error'));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }else{
                return Text("nothing");
              }
            }
        ),
      ),
    );
  }
}
