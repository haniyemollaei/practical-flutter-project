import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:form2/Models/user.dart';
import 'package:form2/service/toast.dart';
import 'package:form2/widgets/custom_button_filled_red.dart';
import 'package:form2/widgets/custom_button_white_filled.dart';
import 'package:form2/widgets/custom_text_field_outline.dart';
import 'package:http/http.dart' as http;
import 'package:form2/sqliteProvider/user_provider.dart';
import 'log_in.dart';
import 'dart:math';

class Register extends StatefulWidget {
  const Register({Key key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKey = GlobalKey<FormState>();
  String username, email, password, pass_tmp;
  var rng = new Random();

  Future<http.Response> createAlbum(String title) async {
    return await http.post(
      Uri.parse('https://jsonplaceholder.typicode.com/albums'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'title': title,
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.jpg"),
              fit: BoxFit.cover,
              //colorFilter: ColorFilter.mode(Colors.white54, BlendMode.hue),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(30.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[

                  ///////////////////////////////////////  email  //////////////////////////////////////
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    child: CustomTextFieldOutline(
                      label: "ایمیل",
                      icon: Icon(Icons.email , color: Colors.orange,),
                      keyboardType: TextInputType.emailAddress,
                      password: false,
                      onSaved: (String str){
                        email = str;
                      },
                    ),
                  ),

                  ///////////////////////////////////////  username  //////////////////////////////////////
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    child: CustomTextFieldOutline(
                      label: "نام کاربری",
                      icon: Icon(Icons.person , color: Colors.orange,),
                      keyboardType: TextInputType.text,
                      password: false,
                      onSaved: (String str){
                        username = str;
                      },
                    ),
                  ),

                  ///////////////////////////////////////  password  //////////////////////////////////////
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    child: CustomTextFieldOutline(
                      label: "رمز عبور",
                      icon: Icon(Icons.workspaces_outline , color: Colors.orange,),
                      keyboardType: TextInputType.text,
                      password: true,
                      onSaved: (String str){
                        password = str;
                      },
                    ),
                  ),

                  ///////////////////////////////////////  re-password  //////////////////////////////////////
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    child: CustomTextFieldOutline(
                      label: "تکرار رمز عبور",
                      icon: Icon(Icons.workspaces_filled , color: Colors.orange,),
                      keyboardType: TextInputType.text,
                      password: true,
                      onSaved: (String str){
                        if(str == this.password){
                          pass_tmp= str;
                        }
                      },
                    ),
                  ),

                  ///////////////////////////////////////  Register Button  //////////////////////////////////////
                  CustomButtonRedFilled(
                    label: "ثبت نام",
                    onPressedDo: () async {
                      if (_formKey.currentState.validate()) {
                        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //     content: Text('اطلاعات در حال پردازش است.')));
                        ShowToastComponent.showDialog("ثبت نام انجام شد",  context);
                        _formKey.currentState.save();
                        saveData( rng.nextInt(90)+10 , email, username, password);
                        await createAlbum("hello ${rng.nextInt(100)}");
                        print('$username   $email   $password   $pass_tmp');
                      }
                    },
                  ),
                  ///////////////////////////////////////  Back Button  //////////////////////////////////////
                  CustomButtonWhiteFilled(
                    label: "بازگشت",
                    onPressedDo: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LogIn()),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> saveData(int id , String email, String username, String password) async {

    var db = new UserProvider();
    await db.openUsers();
    await db.insert(new User(id, email, username, password));
    User temp = await db.getUser(username);
    print("********** user is registered : ${temp.email}   ********** ");
    await db.close();
  }
}
