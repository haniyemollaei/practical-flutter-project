import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as img;
import 'package:form2/service/image_byte_service.dart';
import 'Models/image_byte.dart';

class Gallery2 extends StatefulWidget {
  const Gallery2({Key key}) : super(key: key);

  @override
  _Gallery2State createState() => _Gallery2State();
}

class _Gallery2State extends State<Gallery2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.amber,
      ),
      child: FutureBuilder<List<ImageByte>>(
        future: ImageByteService.getAllImages(),
        initialData: [],
        builder: (context, AsyncSnapshot<List<ImageByte>> snapshot) {
          return snapshot.hasData
              ? GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
              ) ,
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  color: Colors.white ,
                  child: Center(
                      child: Image.memory(snapshot.data[index].imageBytes),

                  ),
                );
              }
          ) : Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
