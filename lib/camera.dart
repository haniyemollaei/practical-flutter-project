import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form2/Models/image.dart';
import 'package:form2/Models/image_byte.dart';
import 'package:form2/sqliteProvider/image_byte_provider.dart';
import 'package:form2/sqliteProvider/image_provider.dart';
import 'package:form2/service/constants.dart';
import 'package:form2/widgets/custom_button_filled_red.dart';
import 'package:form2/widgets/custom_button_white_filled.dart';

import 'gallery.dart';
import 'main.dart';

class CameraPage extends StatefulWidget {

  final CameraDescription camera;
  const CameraPage(  this.camera, {Key key}) : super(key: key);

  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {

  CameraController _controller;
  Future<void> _initializeControllerFuture;

  @override
  void initState() {

    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      widget.camera,
      // Define the resolution to use.
      ResolutionPreset.medium,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('عکس گرفتن')),
      // You must wait until the controller is initialized before displaying the
      // camera preview. Use a FutureBuilder to display a loading spinner until the
      // controller has finished initializing.
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // If the Future is complete, display the preview.
            return CameraPreview(_controller);
          } else {
            // Otherwise, display a loading indicator.
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        // Provide an onPressed callback.
        onPressed: () async {
          // Take the Picture in a try / catch block. If anything goes wrong,
          // catch the error.
          try {
            // Ensure that the camera is initialized.
            await _initializeControllerFuture;

            // Attempt to take a picture and get the file `image`
            // where it was saved.
            final image = await _controller.takePicture();
            final path = image.path;
            final bytes = await image.readAsBytes();
            // If the picture was taken, display it on a new screen.
            await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(
                  // Pass the automatically generated path to
                  // the DisplayPictureScreen widget.
                  imagePath: path,
                  imageBytes: bytes,
                ),
              ),
            );
          } catch (e) {
            // If an error occurs, log the error to the console.
            print(e);
          }
        },
        child: const Icon(Icons.camera_alt),
      ),
    );

  }
}

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;
  final Uint8List imageBytes;
  const DisplayPictureScreen({Key key, this.imagePath, this.imageBytes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: const Text('Display the Picture')),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Center(child: Column(
        children: [
          Flexible(child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.file(File(imagePath)),
          ),flex: 8,),
          Flexible(
            flex: 1,
            child: CustomButtonRedFilled(
              label: "ذخیره در گالری",
              onPressedDo: () {
                saveToGallery(
                    Constants.IMAGE_ID ++, File(imagePath).path.toString());
                saveToGallery2(Constants.IMAGE_ID++, imageBytes);
                print('*********  Image id :   ${Constants.IMAGE_ID}');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Gallery()),
                );
              },
            ),
          ),
          Flexible(
            flex: 1,
            child: CustomButtonWhiteFilled(
              label: "بازگشت",
              onPressedDo: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyHomePage()),
                );
              },
            ),
          ),
        ],
      )),
    );
  }

  Future<void> saveToGallery(int id, String imagePath) async {
    var db = new PhotoProvider();
    await db.openImages();
    await db.insert(new CustomImage(id: id, path: imagePath));
    CustomImage temp = await db.getImage(id);
    print("********** Image is stored : ${temp.path}   ********** ");
    await db.close();



  }

  Future<void> saveToGallery2(int id, Uint8List imageBytes) async {
    var db2 = new ImageByteProvider();
    await db2.openImageBytes();
    await db2.insert(new ImageByte(id: id, imageBytes: imageBytes));
    ImageByte temp = await db2.getImage(id);
    print("********** Image is stored : ${temp.id}     ${temp.imageBytes}   ********** ");
    await db2.close();
  }
}