import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:form2/Models/post.dart';
import 'package:form2/api_users.dart';
import 'package:form2/main.dart';
import 'package:form2/service/post_service.dart';
import 'package:form2/service/user_service.dart';
import 'package:form2/widgets/custom_button_filled_red.dart';
import 'package:form2/widgets/custom_button_white_filled.dart';
import 'package:form2/widgets/custom_text_field_outline.dart';
import 'package:form2/widgets/textArea.dart';
import 'package:http/http.dart' as http;
import 'Models/user.dart';
import 'feed.dart';

class SendPost extends StatefulWidget {
  const SendPost({Key key}) : super(key: key);

  @override
  _SendPostState createState() => _SendPostState();
}

class _SendPostState extends State<SendPost> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  var rng = new Random();
  var title, body;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.jpg"),
              fit: BoxFit.cover,
              //colorFilter: ColorFilter.mode(Colors.white54, BlendMode.hue),
            ),
          ),
          child: Padding(
            key: _scaffoldKey,
            padding: const EdgeInsets.all(30.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 10),
                    child: CustomTextFieldOutline(
                      label: "عنوان پست",
                      icon: Icon(Icons.title),
                      keyboardType: TextInputType.text,
                      password: false,
                      onSaved: (String str) {
                        title = str;
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 5, 20, 20),
                    child: CustomTextArea(
                      label: 'متن ویرایش شده ...',
                      onSaved: (text) {
                        body = text;
                      },
                      icon: Icon(Icons.text_fields),
                    ),
                  ),
                  CustomButtonRedFilled(
                    label: "ارسال",
                    onPressedDo: () async {
                      _formKey.currentState.save();
                      var response = await PostService.createPost(new Post(
                          (rng.nextInt(900) + 100),
                          (rng.nextInt(900) + 100),
                          title,
                          body));

                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Feed()),
                      );
                    },
                  ),
                  CustomButtonWhiteFilled(
                    label: "بازگشت",
                    onPressedDo: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MyHomePage()),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }
}
