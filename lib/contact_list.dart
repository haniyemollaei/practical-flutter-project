import 'package:flutter/material.dart';
import 'package:form2/Models/user.dart';
import 'package:form2/service/user_service.dart';

class ContactList extends StatefulWidget {
  const ContactList({Key key}) : super(key: key);

  @override
  _ContactListState createState() => _ContactListState();
}

class _ContactListState extends State<ContactList> {

  @override
  build(BuildContext context) {

    // Build a Form widget using the _formKey created above.
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Contacts'),
        ),
        body: FutureBuilder<List<User>>(
          future: UserService.getAllUsers(),
          initialData: [],
          builder: (context, AsyncSnapshot<List<User>> snapshot) {
            return snapshot.hasData
                ? ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (_, int position) {
                      final item = snapshot.data[position];
                      //get your item data here ...
                      return Card(
                        child: ListTile(
                          title:
                              Text("user: " + item.email),
                        ),
                      );
                    },
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  );
          },
        ),
      ),
    );
  }
}
