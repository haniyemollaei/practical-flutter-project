import 'package:form2/Models/image.dart';
import 'package:sqflite/sqflite.dart';
import 'provider.dart';

class PhotoProvider extends Provider{

  String _tableName = 'images';

  Future<bool> insert( CustomImage img, { conflictAlgorithm: ConflictAlgorithm.replace }) async {
    try {
      await db.insert(
          _tableName, img.toJson() , conflictAlgorithm: conflictAlgorithm);
      return true;
    }catch(e){
      print(e);
      return false;
    }
  }



  Future<CustomImage> getImage(int id) async {
    List<Map> maps = await db.query(
        _tableName,
        columns: ['id', 'path'],
        where: 'id = ?',
        whereArgs: [id]
    );

    if (maps.length > 0) {
      return CustomImage.fromJson(maps.first);
    }

    return null;
  }


  Future<int> delete(int id) async {
    return await db.delete(_tableName, where: 'id = ?', whereArgs: [id]);
  }


  Future<int> update(CustomImage img) async {
    return await db.update(
        _tableName, img.toJson(), where: 'id= ?', whereArgs: [img.id]);
  }


  Future<List<CustomImage>> getAllImages() async {
    final db2 = db;
    List<Map> results = await db2.query(
        "images",orderBy: "id ASC"
    );
    List<CustomImage> images = [];
    results.forEach((result) {
      CustomImage img = CustomImage.fromJson(result);
      images.add(img);
    });
    return images;
  }
}