import 'package:form2/Models/user.dart';
import 'package:form2/sqliteProvider/provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class UserProvider extends Provider {


  String _tableName = 'users';

  Future<bool> insert(User user, { conflictAlgorithm: ConflictAlgorithm.replace }) async {
    try {
      await db.insert(
          _tableName, user.toJson(), conflictAlgorithm: conflictAlgorithm);
      return true;
    }catch(e){
      print(e);
    }
  }



  Future<User> getUser(String username) async {
    List<Map> maps = await db.query(
        _tableName,
        columns: ['id', 'email', 'username', 'password'],
        where: 'username = ?',
        whereArgs: [username]
    );

    if (maps.length > 0) {
      return User.fromJson(maps.first);
    }

    return null;
  }


  Future<int> delete(String username) async {
    return await db.delete(_tableName, where: 'username = ?', whereArgs: [username]);
  }


  Future<int> update(User user) async {
    return await db.update(
        _tableName, user.toJson(), where: 'username = ?', whereArgs: [user.username]);
  }


  Future<List<User>> getAllUsers() async {
    final db2 = await db;
    List<Map> results = await db2.query(
        "users",orderBy: "id ASC"
    );
    List<User> users = new List();
    results.forEach((result) {
      User user = User.fromJson(result);
      users.add(user);
    });
    return users;
  }
}