import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class Provider{
  Database db;
  String _path;

  Future openUsers({String dbName : 'users.db'}) async {

    var databasesPath = await getDatabasesPath();
    _path = join(databasesPath, dbName);

    db = await openDatabase(_path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute('''
          create table users ( 
            id integer primary key autoincrement, 
            username text not null,
            email text not null,
            password text not null)
          ''');
        });
  }

  Future openImages({String dbName : 'images.db'}) async {

    var databasesPath = await getDatabasesPath();
    _path = join(databasesPath, dbName);

    db = await openDatabase(_path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute('''
          create table images ( 
            id integer primary key autoincrement, 
            path text not null)
          ''');
        });
  }

  Future openImageBytes({String dbName : 'bytes.db'}) async {

    var databasesPath = await getDatabasesPath();
    _path = join(databasesPath, dbName);

    db = await openDatabase(_path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute('''
          create table bytes ( 
            id integer primary key autoincrement, 
            image_bytes text not null)
          ''');
        });
  }

  Future close() async => db.close();

}