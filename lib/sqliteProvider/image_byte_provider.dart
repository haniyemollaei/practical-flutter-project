
import 'package:form2/Models/image_byte.dart';
import 'package:form2/sqliteProvider/provider.dart';
import 'package:sqflite/sqflite.dart';

class ImageByteProvider extends Provider{

  String _tableName = 'bytes';

  Future<bool> insert( ImageByte img, { conflictAlgorithm: ConflictAlgorithm.replace }) async {
    try {
      await db.insert(
          _tableName, img.toJson() , conflictAlgorithm: conflictAlgorithm);
      return true;
    }catch(e){
      print(e);
      return false;
    }
  }



  Future<ImageByte> getImage(int id) async {
    List<Map> maps = await db.query(
        _tableName,
        columns: ['id', 'image_bytes'],
        where: 'id = ?',
        whereArgs: [id]
    );

    if (maps.length > 0) {
      return ImageByte.fromJson(maps.first);
    }

    return null;
  }


  Future<List<ImageByte>> getAllImages() async {
    final db2 = db;
    List<Map> results = await db2.query(
        "bytes",orderBy: "id ASC"
    );
    List<ImageByte> images = [];
    results.forEach((result) {
      ImageByte img = ImageByte.fromJson(result);
      images.add(img);
    });
    return images;
  }
}