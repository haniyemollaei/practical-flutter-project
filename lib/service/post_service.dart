import 'dart:convert';
import 'package:form2/Models/post.dart';
import 'package:http/http.dart' as http;
class PostService {

  static Future<http.Response> createPost(Post post) {
    return http.post(
      Uri.parse('https://jsonplaceholder.typicode.com/posts'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(post.toJson()),
    );
  }

  static Future<http.Response> updatePost(Post post) {
    return http.put(
      Uri.parse('https://jsonplaceholder.typicode.com/posts/${post.id}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(post.toJson()),
    );
  }

  static List<Post> parsePosts(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Post>((json) =>Post.fromJson(json)).toList();
  }

  static Future<List<Post>> fetchPosts() async {
    final response = await http.get(Uri.parse('https://jsonplaceholder.typicode.com/posts'));
    if (response.statusCode == 200) {
      return parsePosts(response.body);
    } else {
      throw Exception('Unable to fetch posts from the REST API');
    }
  }
}