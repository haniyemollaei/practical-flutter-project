
import 'package:form2/Models/image_byte.dart';
import 'package:form2/sqliteProvider/image_byte_provider.dart';


class ImageByteService {

  static Future<ImageByte> getImage(int id) async {
    var dbImage = new ImageByteProvider();

    try {

      await dbImage.openImages();
      ImageByte img = await dbImage.getImage(id);
      await dbImage.close();

      return img;

    } catch (e) {
      return null;
    }
  }


  static Future<List<ImageByte>> getAllImages() async {
    var dbImage = new ImageByteProvider();

    try{
      await dbImage.openImageBytes();
      List<ImageByte> images = await dbImage.getAllImages();
      await dbImage.close();
      return images;

    }catch(e){
      print(e);
      return null;
    }
  }

}