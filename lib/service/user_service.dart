import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:form2/Models/user.dart';
import 'package:form2/sqliteProvider/user_provider.dart';


class UserService {


  static Future<User> getUser(String username) async {
    var dbUser = new UserProvider();

    try {

      await dbUser.openUsers();
      User user = await dbUser.getUser(username);
      await dbUser.close();

      return user;

    } catch (e) {
      return null;
    }
  }

  static Future updateUser(User user) async {
    var dbUser = new UserProvider();

    await dbUser.openUsers();
    await dbUser.update(user);
    await dbUser.close();
    return true;
  }

  static Future removeUser(String username) async {
    var dbUser = new UserProvider();

    await dbUser.openUsers();
    await dbUser.delete(username);
    await dbUser.close();
    return true;
  }

  static Future<List<User>> getAllUsers() async {
    var dbUser = new UserProvider();

    try{
      await dbUser.openUsers();
      List<User> users = await dbUser.getAllUsers();
      await dbUser.close();
      return users;

    }catch(e){
      print(e);
      return null;
    }
  }


  static List<User> parseUsers(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<User>((json) =>User.fromJson(json)).toList();
  }


  static Future<List<User>> fetchUsers2() async {
    final response = await http.get(Uri.parse('https://jsonplaceholder.typicode.com/users'));
    if (response.statusCode == 200) {
      return parseUsers(response.body);
    } else {
      throw Exception('Unable to fetch products from the REST API');
    }
  }

}