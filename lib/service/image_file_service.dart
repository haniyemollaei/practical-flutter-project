import 'dart:convert';
import 'package:form2/Models/image.dart';
import 'package:form2/sqliteProvider/image_provider.dart';
import 'package:http/http.dart' as http;




class ImageFileService {


  static Future<CustomImage> getImage(int id) async {
    var dbImage = new PhotoProvider();

    try {

      await dbImage.openImages();
      CustomImage img = await dbImage.getImage(id);
      await dbImage.close();

      return img;

    } catch (e) {
      return null;
    }
  }


  static Future<List<CustomImage>> getAllImages() async {
    var dbImage = new PhotoProvider();

    try{
      await dbImage.openImages();
      List<CustomImage> images = await dbImage.getAllImages();
      await dbImage.close();
      return images;

    }catch(e){
      print(e);
      return null;
    }
  }


  static List<CustomImage> parseImages(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<CustomImage>((json) => CustomImage.fromJson(json))
        .toList();
  }
}